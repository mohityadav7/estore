// for categories list
$(document).ready(function() {
  $(".list-toggle").on('click', function(e){
    e.preventDefault();
    $(this).next().toggle();
    $(this).find('.icon').toggleClass('plus');
    $(this).find('.icon').toggleClass('minus');
  });
});

// for sidebar navigation
$(document).ready(function() {
  // hide all forms except first one
  $(".d-toggle").not('#add-category').css('display', 'none');
  // toggle forms from sidebar menu
  $(".sidebar-d-toggle").on('click', function(e) {
    $(this).siblings('.sidebar-d-toggle').removeClass('active-item');
    $(this).addClass('active-item');
    e.preventDefault();
    var id = $(this).children('a')[0].text.toLowerCase().replace(/ /g, "-");
    $('.d-toggle').not('#'+id).css('display', 'none');;
    $('#'+id).css('display', 'inherit');
  });
});
